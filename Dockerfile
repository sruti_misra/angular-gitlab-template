FROM node:8-alpine
MAINTAINER Sruti Misra (sruti.misra@infostretch.com)
RUN npm install -g @angular/cli && \
    mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY . /usr/src/app
RUN npm install
EXPOSE 4200
CMD [ "ng", "serve","--host", "0.0.0.0" ]
